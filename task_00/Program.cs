﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_00
{
    class Program
    {
        static class Calculator
        {
            public static double Sum(double a, double b)
            {
                return a + b;
            }

            public static double Subtract(double a, double b)
            {
                return a - b;
            }

            public static double Mult(double a, double b)
            {
                return a * b;
            }

            public static double Divide(double a, double b)
            {
                if (b == 0)
                {
                    Console.WriteLine("Can not be divided by zero");
                    return 0;
                }

                return a / b;
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("{0} + {1} = {2}", 4, 5, Calculator.Sum(4, 5));
            Console.WriteLine("{0} - {1} = {2}", 4, 5, Calculator.Subtract(4, 5));
            Console.WriteLine("{0} * {1} = {2}", 4, 5, Calculator.Mult(4, 5));
            Console.WriteLine("{0} / {1} = {2}", 4, 5, Calculator.Divide(4, 5));
            Console.WriteLine("{0} / {1} = {2}", 4, 5, Calculator.Divide(4, 0));

            Console.ReadKey();
        }
    }
}
